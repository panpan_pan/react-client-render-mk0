import React, { useState } from "react";
import { connect } from "react-redux";
import hboApi from "../api/apiConfig";
import apiList, { baseUrl } from "../api/posamanConfig.json";
import { Row, Col, Button, Input } from "antd";
import "../antd.css";

function ApiButton({ url, rawModeData = "", name, setApiname }) {
  const postParams = rawModeData === "" ? {} : JSON.parse(rawModeData);
  const [postData, setPostData] = useState(postParams);

  const apiSend = () => {
    const apiResource = url.replace(baseUrl, "");
    setApiname(name);
    hboApi.post(apiResource, postData);
  };

  const inputChange = e =>
    setPostData({
      ...postData,
      [e.target.name]: e.target.value
    });

  return (
    <div style={{ marginBottom: "50px" }}>
      <Button type="primary" onClick={apiSend}>
        {name}
      </Button>
      <br />
      <ul>
        {Object.keys(postData).map((key, i) => (
          <li key={`${i}_li`}>
            {`${key}:`}
            <Input value={postData[key]} name={key} onChange={inputChange} />
          </li>
        ))}
      </ul>
    </div>
  );
}

function Index({ apiResponse: { msg } }) {
  const [apiName, setApiname] = useState("");
  const filterList = data => {
    const disableList = ["Login"];
    return !disableList.find(val => data.name === val);
  };
  const { requests } = apiList;
  
  return (
    <>
      <h2>Api test</h2>
      <Row gutter={16}>
        <Col sm={8} xs={24}>
          {requests.filter(filterList).map((data, i) => {
            const props = {
              ...data,
              setApiname
            };
            return <ApiButton {...props} key={`${i}_list`} />;
          })}
        </Col>
        <Col sm={16} xs={24}>
          <h3>{apiName}</h3>
          {msg}
        </Col>
      </Row>
    </>
  );
}

export default connect(state => ({ apiResponse: state.apiResponse }))(Index);
