import React, { useState } from "react";
import { connect } from "react-redux";
import { accounts } from "../redux/actions/account";
import { Input, Button } from "antd";
import hboApi from "../api/apiConfig";

import "../antd.css";

function Index({ account: { num }, apiResponse: { msg }, accontAction }) {
  const [account, setAccount] = useState("");
  const [password, setPassword] = useState("");
  const login = () => {
    hboApi
      .post(`AppService/Login`, {
        Account: account,
        Password: password
      })
      .then(function(res) {
        const { access_token, refresh_token } = res.data;
        if (access_token && refresh_token) {
          localStorage.setItem("refresh_token", refresh_token);
          localStorage.setItem("access_token", access_token);
        } else {
          localStorage.removeItem("refresh_token");
          localStorage.removeItem("access_token");
        }
      });
  };

  const getUserContent = () => {
    hboApi.post(`AppService/GetUserContent`);
  };

  const refreshToken = () => {
    //
  };

  const action = () => {
    accontAction();
  };

  const style = {
    width: "300px",
    textAlign: "left"
  };
  return (
    <>
      <h2>Home</h2>
      <div style={style}>
        <Input
          placeholder="account"
          value={account}
          onChange={e => setAccount(e.target.value)}
        />
        <Input.Password
          placeholder="password"
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
        <p />
        <Button type="primary" onClick={login}>
          Login
        </Button>
        <p />
        <Button type="primary" onClick={getUserContent}>
          GetUserContent
        </Button>
        <p />
        <Button type="primary" onClick={refreshToken}>
          RefreshToken
        </Button>
        <p />
        <Button type="primary" onClick={action}>
          Action
        </Button>
        <p />
        {"redux data:"}
        {num}
        <p />
        {msg}
      </div>
    </>
  );
}

export default connect(
  state => ({
    account: state.account,
    apiResponse: state.apiResponse
  }),
  {
    accontAction: accounts
  }
)(Index);
