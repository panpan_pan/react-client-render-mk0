import React from "react";
import { Redirect } from "react-router";

export default Page => {
  return function page(props) {
    const accessToken = localStorage.getItem("access_token");
    return !accessToken ? <Redirect to="/login/" /> : <Page {...props} />;
  };
};
