import React from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import Index from "./page/index";
import Login from "./page/login";
import ApiTest from "./page/apiTest";
import checkLogin from "./components/checkLogin";
import { Layout, Menu } from "antd";

const { Header, Content } = Layout;

function nav(InnerContent) {
  return function Nav(props) {
    const {
      location: { pathname }
    } = props;
    return (
      <Layout>
        <Header className="header">
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={[pathname]}
            style={{ lineHeight: "64px" }}
          >
            <Menu.Item key="/">
              <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item key="/about/">
              <Link to="/about/">About</Link>
            </Menu.Item>
            <Menu.Item key="/apitest/">
              <Link to="/apitest/">Api test</Link>
            </Menu.Item>
          </Menu>
        </Header>
        <Layout>
          <Content style={{ background: "#fff" }}>
            <InnerContent {...props} />
          </Content>
        </Layout>
      </Layout>
    );
  };
}

function About() {
  return (
    <>
      <h2>About</h2>
      ...........
      <br />
      .....................
      <br />
      <br />
      ................
      <br />
      ...............
      <br />
      .......................
      <br />
      ...........
      <br />
      <br />
      ...........
      <br />
      ...........
      <br />
    </>
  );
}

const NoMatch = Login;
export default function PageRputer() {
  return (
    <Router>
      <Switch>
        <Route path="/login/" component={Login} />
        <Route path="/" exact render={checkLogin(nav(Index))} />
        <Route path="/about/" render={checkLogin(nav(About))} />
        <Route path="/apitest/" render={checkLogin(nav(ApiTest))} />
        <Route component={NoMatch} />
      </Switch>
    </Router>
  );
};
