import axios from "axios";

const baseURL = "http://192.168.102.14:30001";
const hboApi = axios.create({
  baseURL
});

hboApi.interceptors.request.use(
  config => {
    //const refresh_token = localStorage.getItem('refresh_token');
    const access_token = localStorage.getItem("access_token");
    // if (config.method === 'post') {
    //   config.data = qs.stringify(config.data);
    // }
    config.headers["Access-Token"] = access_token;
    return config;
  },
  error => {
    console.log(error);
    Promise.reject(error);
  }
);

export default hboApi;
