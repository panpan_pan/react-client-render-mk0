import { combineReducers } from "redux";
import account from "./reducers/account";
import apiResponse from "./reducers/apiResponse";

export default combineReducers({
  account,
  apiResponse
});
