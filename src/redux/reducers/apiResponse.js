export default function apiResponse(
  state = {
    msg: ''
  }, action) {
  switch (action.type) {
  case 'APIRESPONSE_UPDATE':
    state = { msg: action.msg }
    break;
  default:
  }
  return state;
}