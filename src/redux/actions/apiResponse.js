export function apiResponseUpdate(msg) {
  return function (dispatch) {
    dispatch({
      type: "APIRESPONSE_UPDATE",
      msg,
    })
  }
}