import React from "react";
import { render } from "react-dom";
import PageRouter from "./pageRouter";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import storeData from "./redux/store";
import * as serviceWorker from "./serviceWorker";
import hboApi from "./api/apiConfig";
import { apiResponseUpdate } from "./redux/actions/apiResponse";

const isDevelopment = process.env.NODE_ENV === "development";
const store = isDevelopment
  ? createStore(storeData, applyMiddleware(thunk, logger))
  : createStore(storeData, applyMiddleware(thunk));

hboApi.interceptors.response.use(
  function(response) {
    const { dispatch } = store;
    dispatch(apiResponseUpdate(JSON.stringify(response)));
    return response;
  },
  function(error) {
    const { dispatch } = store;
    dispatch(apiResponseUpdate(JSON.stringify(error)));
    return Promise.reject(error);
  }
);

render(
  <Provider store={store}>
    <PageRouter />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
